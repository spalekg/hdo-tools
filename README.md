# Rigol HDO-series license generator

* Obtain the IP address of the scope
* Connect to the scope using the Android Debug Bridge `adb connect <IP of scope>:55555`
* Download the unique key from the scope `adb pull /rigol/data/Key.data`
* Generate the license key using the rgtool program on the Key file `go run ./rgtool.go`
* Insert generated codes via SCPI interface on the website on the scope (`http://<IP of scope>`)

When not wanting to install adb or go, docker could also be used instead.
```sh
docker run --rm -it -v $(pwd):/workdir -w /workdir alpine \
  /bin/sh -c 'apk add android-tools && adb connect <IP of scope>:55555 && adb pull /rigol/data/Key.data'
```
```sh
docker run --rm -it -v $(pwd):/workdir -w /workdir golang go run ./rgtool.go
```
